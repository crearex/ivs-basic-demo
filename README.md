## virtú basic player example
\
Adapted basic AWS example, added our playback URL. \
Just view index.html in a browser. 


## Installing

* Connect to your ec2 instance

* Install RPMs
 
  ```bash 
  sudo yum install -y nano git amazon-linux-extras httpd
  ```
* Clone Repository
 
   ```bash
    cd ~; 
    git clone https://gitlab.com/creare-x/ivs-basic-demo.git www
    ```
   Enter your credentials as needed. 
* Copy your files to /var/www/html
* Start httpd
  ```bash
  sudo systemctl httpd enable 
  sudo systemctl httpd start
  ```