import config from './config.js';

export default class {
    constructor(playbackUrl) {
        this.playbackUrl = playbackUrl;
        this.playerElementName = config.player.DOMElement;

        try {
            registerIVSTech(videojs);
            registerIVSQualityPlugin(videojs);
        } catch (e) {
            console.log('There was an error referencing videoJs. Please load it before. ');
            return false;
        }

        this.videoJSPlayer = videojs(this.playerElementName, {
            techOrder :["AmazonIVS"],
            controlBar : {
                playToggle : {
                    replay: false
                }, 
                pictureInPictureToggle: false
            }
        });

        this.initPlayer();
    }

    initPlayer() {
        const PlayerState = this.videoJSPlayer.getIVSEvents().PlayerState;
        const PlayerEventType = this.videoJSPlayer.getIVSEvents().PlayerEventType;

        window.videoJSPlayer = this.videoJSPlayer;
        const ivsPlayer = this.videoJSPlayer.getIVSPlayer();
        const videoContainerEl = document.querySelector("#" + this.playerElementName);
        videoContainerEl.addEventListener("click", () => {
            if (this.videoJSPlayer.paused()) {
                videoContainerEl.classList.remove("vjs-has-started");
            } else {
                videoContainerEl.classList.add("vjs-has-started");
            }
        });
        
        // Log playstate
        ivsPlayer.addEventListener(PlayerState.PLAYING, () => {
            if (config.player.log.playing) {
                console.info("Player State - PLAYING");
                setTimeout(() => {
                    console.info(
                        `This stream is ${
                            ivsPlayer.isLiveLowLatency() ? "" : "not "
                        }playing in ultra low latency mode`
                    );
                    console.info(`Stream Latency: ${ivsPlayer.getLiveLatency()}s`);
                }, 5000);
            }
        });
  
        // Log errors
        if (config.player.log.errors) {
            ivsPlayer.addEventListener(PlayerEventType.ERROR, (type, source) => {
                console.warn("Player Event - ERROR: ", type, source);
            });
        }


        if (config.player.log.textMetadataCue) {
            // Log and display timed metadata
            ivsPlayer.addEventListener(PlayerEventType.TEXT_METADATA_CUE, (cue) => {
                const metadataText = cue.text;
                const position = ivsPlayer.getPosition().toFixed(2);
                console.log(
                    `Player Event - TEXT_METADATA_CUE: "${metadataText}". Observed ${position}s after playback started.`
                );
            });
        }
  
        // Enables manual quality selection plugin
        if (config.player.enableIVSQualityPlugin) {
            this.videoJSPlayer.enableIVSQualityPlugin();
        }
  
        // Set volume and play default stream
        this.videoJSPlayer.volume(config.player.startVolume);
        this.videoJSPlayer.src(this.playbackUrl);
    }
}

