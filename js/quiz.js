export default class {
    constructor(wrapper) {
        this.wrapperEl = document.getElementById('#' + wrapper);
        this.answerEl = this.wrapperEl.querySelector('#answers');
        this.questionEl = this.wrapperEl.querySelector('#question');
        this.standbyEl = this.wrapperEl.querySelector('#standby');
        this.endpoint = '';
    }

    init = function(json) {
        let conf = JSON.parse(json);
        let endpoint;

        try {
            endpoint = new URL(conf.endpoint);
          } catch (_) {
            return false;  
          }

          this.endpoint = endpoint;
    }

    recieveQuestion = function(json) {
        let obj = JSON.parse(json);

        if (obj.type === "quiz") {
            let question = obj.question || null;
            let answers = obj.answers || [];
            this.showQuestion(question, answers);
        }
    }

    showQuestion = function(question, answers) {
        
        // clear existing answers
        while (this.answerEl.firstChild) {
            this.answerEl.removeChild(this.answerEl.firstChild);
        }

        // set headline
        this.wrapperEl.querySelector('#question').textContent = question.text;

        // create DOM Nodes and append them to parent
        
            
        
    }

    createAnswer = function (answer) {
        let link = document.createElement("a");
        link.dataset.add('id', answer.id);
        link.classList.add('answer');

        let linkTextNode = document.createTextNode(answer.text);
        link.appendChild(linkTextNode);
        this.answerEl.appendChild(link);

        link.addEventListener('click', (event) => {
            event.target.classList.add('clicked');
            let answerId = event.target.dataset.id;

            setTimeout(function () {
                this.sendAnswer(answerId);
                this.answerEl.hide();
                this.standbyEl.style.display = "";
                event.target.classList.remove('clicked');
              }, 1050);
        });

    }

    sendAnswer = async function(data) {
        const response = await fetch(this.endpoint, {
            method: POST,
            headers: {
                'Content-type':'application/json'
            },
            body: JSON.stringify(data)

        })
        .then(data => {
            document.getElementById('#feedback').textContent = data.message;
        });
    }
}