export default {
    player: {
        DOMElement: 'ivsPlayerElement',
        startVolume: 0.5,
        enableIVSQualityPlugin: true,
        log: {
            textMetadataCue: true,
            errors: true,
            playing: false
        }
    }
}