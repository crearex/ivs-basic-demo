/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./js/config.js":
/*!**********************!*\
  !*** ./js/config.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({\n  player: {\n    DOMElement: 'ivsPlayerElement',\n    startVolume: 0.5,\n    enableIVSQualityPlugin: true,\n    log: {\n      textMetadataCue: true,\n      errors: true,\n      playing: false\n    }\n  }\n});\n\n//# sourceURL=webpack://ivs-demo/./js/config.js?");

/***/ }),

/***/ "./js/main.js":
/*!********************!*\
  !*** ./js/main.js ***!
  \********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _player_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./player.js */ \"./js/player.js\");\nfunction _typeof(obj) { \"@babel/helpers - typeof\"; if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\n\n\n(function () {\n  console.log(typeof videojs === \"undefined\" ? \"undefined\" : _typeof(videojs));\n  new _player_js__WEBPACK_IMPORTED_MODULE_0__.default('https://fcc3ddae59ed.us-west-2.playback.live-video.net/api/video/v1/us-west-2.893648527354.channel.DmumNckWFTqz.m3u8');\n})();\n\n//# sourceURL=webpack://ivs-demo/./js/main.js?");

/***/ }),

/***/ "./js/player.js":
/*!**********************!*\
  !*** ./js/player.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ _default)\n/* harmony export */ });\n/* harmony import */ var _config_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./config.js */ \"./js/config.js\");\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\n\n\nvar _default = /*#__PURE__*/function () {\n  function _default(playbackUrl) {\n    _classCallCheck(this, _default);\n\n    this.playbackUrl = playbackUrl;\n    this.playerElementName = _config_js__WEBPACK_IMPORTED_MODULE_0__.default.player.DOMElement;\n\n    try {\n      registerIVSTech(videojs);\n      registerIVSQualityPlugin(videojs);\n    } catch (e) {\n      console.log('There was an error referencing videoJs. Please load it before. ');\n      return false;\n    }\n\n    this.videoJSPlayer = videojs(this.playerElementName, {\n      techOrder: [\"AmazonIVS\"],\n      controlBar: {\n        playToggle: {\n          replay: false\n        },\n        pictureInPictureToggle: false\n      }\n    });\n    this.initPlayer();\n  }\n\n  _createClass(_default, [{\n    key: \"initPlayer\",\n    value: function initPlayer() {\n      var _this = this;\n\n      var PlayerState = this.videoJSPlayer.getIVSEvents().PlayerState;\n      var PlayerEventType = this.videoJSPlayer.getIVSEvents().PlayerEventType;\n      window.videoJSPlayer = this.videoJSPlayer;\n      var ivsPlayer = this.videoJSPlayer.getIVSPlayer();\n      var videoContainerEl = document.querySelector(\"#\" + this.playerElementName);\n      videoContainerEl.addEventListener(\"click\", function () {\n        if (_this.videoJSPlayer.paused()) {\n          videoContainerEl.classList.remove(\"vjs-has-started\");\n        } else {\n          videoContainerEl.classList.add(\"vjs-has-started\");\n        }\n      }); // Log playstate\n\n      ivsPlayer.addEventListener(PlayerState.PLAYING, function () {\n        if (_config_js__WEBPACK_IMPORTED_MODULE_0__.default.player.log.playing) {\n          console.info(\"Player State - PLAYING\");\n          setTimeout(function () {\n            console.info(\"This stream is \".concat(ivsPlayer.isLiveLowLatency() ? \"\" : \"not \", \"playing in ultra low latency mode\"));\n            console.info(\"Stream Latency: \".concat(ivsPlayer.getLiveLatency(), \"s\"));\n          }, 5000);\n        }\n      }); // Log errors\n\n      if (_config_js__WEBPACK_IMPORTED_MODULE_0__.default.player.log.errors) {\n        ivsPlayer.addEventListener(PlayerEventType.ERROR, function (type, source) {\n          console.warn(\"Player Event - ERROR: \", type, source);\n        });\n      }\n\n      if (_config_js__WEBPACK_IMPORTED_MODULE_0__.default.player.log.textMetadataCue) {\n        // Log and display timed metadata\n        ivsPlayer.addEventListener(PlayerEventType.TEXT_METADATA_CUE, function (cue) {\n          var metadataText = cue.text;\n          var position = ivsPlayer.getPosition().toFixed(2);\n          console.log(\"Player Event - TEXT_METADATA_CUE: \\\"\".concat(metadataText, \"\\\". Observed \").concat(position, \"s after playback started.\"));\n        });\n      } // Enables manual quality selection plugin\n\n\n      if (_config_js__WEBPACK_IMPORTED_MODULE_0__.default.player.enableIVSQualityPlugin) {\n        this.videoJSPlayer.enableIVSQualityPlugin();\n      } // Set volume and play default stream\n\n\n      this.videoJSPlayer.volume(_config_js__WEBPACK_IMPORTED_MODULE_0__.default.player.startVolume);\n      this.videoJSPlayer.src(this.playbackUrl);\n    }\n  }]);\n\n  return _default;\n}();\n\n\n\n//# sourceURL=webpack://ivs-demo/./js/player.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./js/main.js");
/******/ 	
/******/ })()
;